# Simple Dab is a wrapper around [dab-cmdline](https://github.com/JvanKatwijk/dab-cmdline) to make it easier to use and port to f.e. python
## Build
You first need to install all the prerequisites libfaad, libfftw3, libusb, make, g++, gcc, libsamplerate and librtlsdr.
On Ubuntu install them with:
```
 sudo apt install libfaad-dev libfftw3-dev libusb-1.0-0-dev libsamplerate0-dev librtlsdr-dev g++ make gcc
```
Now you can build the shared object file with `make`.
This should result in a file called `libsimple_dab.so`.
To Install you can also run `sudo make install`

## License
Simple Dab is licensed under GPL-2.0.
