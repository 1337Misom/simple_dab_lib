#include "dab-api.h"

#ifndef BAND_III
#define BAND_III 0100
#endif
#ifndef L_BAND
#define L_BAND 0101
#endif

#define API_Callbacks API_struct

#ifdef DEBUG
#define SDAB_DEBUG_PRINT(...) \
  printf("[DEBUG][%s:%d](in %s): ",__FILE__,  __LINE__, __FUNCTION__), \
  printf(__VA_ARGS__)
#else

#define SDAB_DEBUG_PRINT(...) while(0);

#endif

typedef enum {
  RTLSDR
} DeviceType;

typedef enum {
  OK,
  INIT_ALREADY_DONE, // library already initalized
  UNKNOWN_DEVICE_TYPE, // device not in enum
  DEVICE_INIT_ERROR, // error while initalizing the SDR
  INIT_DAB_ERROR, // dabInit failed
  STATION_NOT_USABLE,
  DEVICE_NOT_INIT
} SDAB_Error;

extern "C" {
  SDAB_Error SDAB_Init(DeviceType device_type, API_Callbacks * callbacks, int16_t new_ppmCorrection = 0);
  SDAB_Error SDAB_Start(uint8_t	new_band,char * new_channel);
  SDAB_Error SDAB_Exit();

  SDAB_Error SDAB_SwitchFrequency(int8_t new_band,char * new_channel);
  SDAB_Error SDAB_SwitchStationName(char* service_name);

  SDAB_Error SDAB_SetGain(int16_t	new_gain);
  SDAB_Error SDAB_SetAutoGain(bool new_autogain);
  bool SDAB_IsAudio(char * service_name);

  void SDAB_DummySyncFunc(bool ignore, void* ignore1){ return; };
  void SDAB_DummySystemFunc(bool ignore, short int ignore1 , int ignore2, void* ignore3){ return; };
  void SDAB_DummyEnsembleFunc(const char* ignore, int ignore1, void* ignore2){ return; };
  void SDAB_DummyFibFunc(short int ignore, void* ignore1){ return; };
  void SDAB_DummyDataFunc(const char* ignore, void* ignore1){ return; };
  void SDAB_DummyBytesFunc(unsigned char* ignore, short int ignore1, unsigned char ignore2, void* ignore3){ return; };
  void SDAB_DummyProgQualFunc(short int ignore, short int ignore1, short int ignore2, void* ignore3){ return; };
  void SDAB_DummyProgdataFunc(audiodata* ignore, void* ignore1){ return; };
  void SDAB_DummyMotdataFunc(uint8_t* ignore, int ignore1, char* ignore2, int ignore3,void* ignore4){ return; };
  void SDAB_DummyTiidataFunc(int ignore, void* ignore1){ return; };
  void SDAB_DummyTimeFunc(int ignore, int ignore1, void* ignore2){ return; };
}
