#include "dab-api.h"
#include "device-exceptions.h"
#include "rtlsdr-handler.h"
#include "device-handler.h"
#include "band-handler.h"
#include "simple_dab.h"
#include "unistd.h"

static void	*dabClass	= NULL;
deviceHandler *sdrDevice;
bandHandler dabBand;

bool init_done = false;

std::string	channel	= "5A";

uint8_t	band = BAND_III;
int16_t	ppmCorrection	= 0;
int16_t	gain = 50;

bool autogain = false;

int32_t frequency	= dabBand.Frequency(band, channel);

bool stop = false;

SDAB_Error SDAB_Init(DeviceType device_type, API_Callbacks* callbacks, int16_t new_ppmCorrection){
  if (init_done){
    return INIT_ALREADY_DONE;
  }
  init_done = false;
  try{
    switch(device_type){
      case RTLSDR:
        sdrDevice = new rtlsdrHandler (frequency,new_ppmCorrection,gain,autogain);
        break;
      default:
        return UNKNOWN_DEVICE_TYPE;
    }
  } catch(std::exception& ex){
    SDAB_DEBUG_PRINT("Exception occured %s\n",ex.what());
    return DEVICE_INIT_ERROR;
  }
  ppmCorrection = new_ppmCorrection;
  dabClass = dabInit(sdrDevice, callbacks,
                     NULL, // no spectrum shown
                     NULL, // no constellations
                     NULL  // Ctx
  );
  if (dabClass == NULL) {
    return INIT_DAB_ERROR;
  }

  init_done = true;
  return OK;
}

SDAB_Error SDAB_Start(uint8_t	new_band,char * new_channel){
  sdrDevice->setGain(gain);

  if (autogain){
    sdrDevice->set_autogain(autogain);
  }
  frequency = dabBand.Frequency(new_band,new_channel);
  sdrDevice->restartReader(frequency);
  dabStartProcessing(dabClass);
  return OK;
}

SDAB_Error SDAB_SwitchFrequency(int8_t new_band,char * new_channel){
  frequency = dabBand.Frequency(new_band, new_channel);
  dabStop(dabClass);
  sdrDevice -> stopReader();
  sdrDevice -> restartReader(frequency);
  dabReset(dabClass);
  return OK;
}


bool SDAB_IsAudio(char * service_name){
  return is_audioService(dabClass,service_name);
}


SDAB_Error SDAB_SwitchStationName(char * service_name){
  SDAB_DEBUG_PRINT("Switching to %s\n",service_name);

  if (!is_audioService(dabClass, service_name)){
    return STATION_NOT_USABLE;
  }

  audiodata ad;

  dataforAudioService(dabClass,service_name,&ad,0);

  if(!ad.defined){
    return STATION_NOT_USABLE;
  }

  dabReset_msc(dabClass);
  set_audioChannel(dabClass,&ad);

  return OK;
}

SDAB_Error SDAB_Exit(){
  if (!init_done){
    return OK;
  }
  sdrDevice->stopReader();
	dabExit(dabClass);
  init_done = false;
  return OK;
}

SDAB_Error SDAB_SetGain(int16_t	new_gain){
  gain = new_gain;
  sdrDevice -> setGain(gain);
  return OK;
}

SDAB_Error SDAB_SetAutoGain(bool new_autogain){
  autogain = new_autogain;
  sdrDevice -> set_autogain(autogain);
  return OK;
}
