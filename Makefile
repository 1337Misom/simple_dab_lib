# Directories
DABCMD_DIR := dab-cmdline
DABCMD_LIB_DIR := $(DABCMD_DIR)/library
DABCMD_DEVICE_DIR := $(DABCMD_DIR)/devices
OBJ_DIR := objs

# Find all .cpp files recursively
SRCS := $(shell find $(DABCMD_LIB_DIR) -type f  \( -iname \*.cpp -o -iname \*.c \) -not -path "*/spiral-sse.c" -not -path '*/src/backend/data/epg/*' )
INCLUDE_DIRS_DEVICES := $(addprefix -I, $(shell find $(DABCMD_DEVICE_DIR) -type f -name '*.h' -exec dirname {} \; | sort -u))
INCLUDE_DIRS_LIB := $(addprefix -I, $(shell find $(DABCMD_LIB_DIR) -type f -name '*.h' -exec dirname {} \; | sort -u))

INCLUDE_DIRS := $(INCLUDE_DIRS_DEVICES) $(INCLUDE_DIRS_LIB) -I $(DABCMD_DIR)
# Generate object file names
OBJS := $(patsubst $(DABCMD_LIB_DIR)/%.cpp,$(OBJ_DIR)/%.o,$(SRCS))
OBJSC := $(patsubst $(DABCMD_LIB_DIR)/%.c,$(OBJ_DIR)/%.o,$(SRCS))
# Compiler and flags
CXX := g++
CXXFLAGS := -std=c++11 -Wall -fPIC

CC := gcc
CFLAGS := -Wall -fPIC

SHARED_LINKS := -lfftw3f -lfaad

# Make targets
.PHONY: all clean

all: library


$(OBJ_DIR)/%.o: $(DABCMD_LIB_DIR)/%.cpp
	$(CXX) $(CXXFLAGS) -c $< $(INCLUDE_DIRS) -o $(OBJ_DIR)/$(notdir $@)

$(OBJ_DIR)/%.o: $(DABCMD_LIB_DIR)/%.c
	$(CC) $(CFLAGS) -c $< $(INCLUDE_DIRS) -o $(OBJ_DIR)/$(notdir $@)

$(OBJ_DIR):
	mkdir -p $(OBJ_DIR)

library: $(OBJ_DIR) $(OBJS) $(OBJSC) device_handler rtlsdr_handler
	$(CXX) $(CXXFLAGS) -fPIC -shared simple_dab.cpp $(OBJ_DIR)/device_handler.o $(OBJ_DIR)/spiral-no-sse.o $(filter-out %.c,$(addprefix $(OBJ_DIR)/, $(notdir $(OBJS)))) $(OBJ_DIR)/rtlsdr_handler.o $(INCLUDE_DIRS) $(SHARED_LINKS) -o libsimple_dab.so

device_handler:
	$(CXX) $(CXXFLAGS) -fPIC -c -o $(OBJ_DIR)/device_handler.o $(INCLUDE_DIRS) $(DABCMD_DEVICE_DIR)/device-handler.cpp

rtlsdr_handler:
	$(CXX) $(CXXFLAGS) -fPIC -c -o $(OBJ_DIR)/rtlsdr_handler.o $(INCLUDE_DIRS) $(DABCMD_DEVICE_DIR)/rtlsdr-handler/rtlsdr-handler.cpp

install: library
	@echo "Installing libsimple_dab"
	@cp libsimple_dab.so /usr/local/lib/
	@echo "Installation complete"
uninstall:
		@echo "Uninstalling libsimple_dab"
		@rm -rf /usr/local/lib/libsimple_dab.so
		@echo "Uninstall complete"

clean:
	rm -rf $(OBJ_DIR)
	rm -rf libsimple_dab.so
